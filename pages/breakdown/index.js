import React, { useEffect, useState, useContext } from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap';
import moment from 'moment';
import PieChart from '../../components/PieChart';

import View from '../../components/View';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';

import { colorRandomizer } from '../../helpers';

export default function index(){
	const [arrayTotal, setArrayTotal] = useState([]);

	const [incTotal, setIncTotal] = useState(0);
	const [expTotal, setExpTotal] = useState(0);
	const [incType, setIncType] = useState(0);
	const [expType, setExpType] = useState(0);

	const [bgColors, setBgColors] = useState([]);

	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	const [expStartDate, setExpStartDate] = useState('');
	const [expEndDate, setExpEndDate] = useState('');


	const {user, setUser} = useContext(UserContext);

	useEffect(() => {
 
        fetch(`${AppHelper.API_URL }/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
        	console.log(data)
            if(data._id){ //JWT validated

                let arrayData = []
                for(let i = 0; i < data.income.length; i++){
	                data.income[i].details.map(res => {
	                	return arrayData.push(res)
	                })
            	}
                for(let i = 0; i < data.expense.length; i++){
	                data.expense[i].details.map(res => {
	                	return arrayData.push(res)
	                })
            	}
            	let sortedArray = arrayData.sort(function(a,b){
				  // Turn your strings into dates, and then subtract them
				  // to get a value that is either negative, positive, or zero.
				  return new Date(a.dateMade) - new Date(b.dateMade);
				});           	
                setArrayTotal(sortedArray)

                let runningAmt = []
                let runningDt = []
                let totalAmt = 0

                let runningAmtMonthly = []
                let runningDtMonthly = []

                let runningAmtMonthlyExp = []
                let runningDtMonthlyExp = []

                sortedArray.map(run => {
                if((moment(run.dateMade).format("YYYY-MM-DD") >= startDate && moment(run.dateMade).format("YYYY-MM-DD") <= moment(run.dateMade).format("YYYY-MM-DD")) && moment(run.dateMade).format("YYYY-MM-DD") <= endDate){	

                	if(run.classification === "Income"){
	                	
 	                	if(!runningDtMonthly.includes(run.category)){
 	                		if(runningAmtMonthly.length > 0){
 	                		runningAmtMonthly.push(parseInt(run.amount))
		                	runningDtMonthly.push(run.category)
 	                		}else{
 	                		runningAmtMonthly.push(parseInt(run.amount))
		                	runningDtMonthly.push(run.category)
		                	}
		                }else{
		                	runningAmtMonthly[runningDtMonthly.indexOf(run.category)] = 
		                	runningAmtMonthly[runningDtMonthly.indexOf(run.category)] + parseInt(run.amount)
		                	
		                }	                  		
		            }//else{

		            	// if(!runningDtMonthlyExp.includes(run.category)){
 	             //    		if(runningAmtMonthlyExp.length > 0){
 	             //    		runningAmtMonthlyExp.push(parseInt(run.amount))
		             //    	runningDtMonthlyExp.push(run.category)
 	             //    		}else{
 	             //    		runningAmtMonthlyExp.push(parseInt(run.amount))
		             //    	runningDtMonthlyExp.push(run.category)
		             //    	}
		             //    }else{
		             //    	runningAmtMonthlyExp[runningDtMonthlyExp.indexOf(run.category)] = 
		             //    	runningAmtMonthlyExp[runningDtMonthlyExp.indexOf(run.category)] + parseInt(run.amount)
		                	
		             //    }	              

		            //}
                }	
                })

                sortedArray.map(run => {
                if((moment(run.dateMade).format("YYYY-MM-DD") >= expStartDate && moment(run.dateMade).format("YYYY-MM-DD") <= moment(run.dateMade).format("YYYY-MM-DD")) && moment(run.dateMade).format("YYYY-MM-DD") <= expEndDate){	

                	if(run.classification === "Expense"){
	                	
 	                // 	if(!runningDtMonthly.includes(run.category)){
 	                // 		if(runningAmtMonthly.length > 0){
 	                // 		runningAmtMonthly.push(parseInt(run.amount))
		                // 	runningDtMonthly.push(run.category)
 	                // 		}else{
 	                // 		runningAmtMonthly.push(parseInt(run.amount))
		                // 	runningDtMonthly.push(run.category)
		                // 	}
		                // }else{
		                // 	runningAmtMonthly[runningDtMonthly.indexOf(run.category)] = 
		                // 	runningAmtMonthly[runningDtMonthly.indexOf(run.category)] + parseInt(run.amount)
		                	
		                // }	                  		
		            // }else{

		            	if(!runningDtMonthlyExp.includes(run.category)){
 	                		if(runningAmtMonthlyExp.length > 0){
 	                		runningAmtMonthlyExp.push(parseInt(run.amount))
		                	runningDtMonthlyExp.push(run.category)
 	                		}else{
 	                		runningAmtMonthlyExp.push(parseInt(run.amount))
		                	runningDtMonthlyExp.push(run.category)
		                	}
		                }else{
		                	runningAmtMonthlyExp[runningDtMonthlyExp.indexOf(run.category)] = 
		                	runningAmtMonthlyExp[runningDtMonthlyExp.indexOf(run.category)] + parseInt(run.amount)
		                	
		                }	              

		            }
                }	
                })

                setBgColors(sortedArray.map(() => `#${colorRandomizer()}`))

                setIncTotal(runningAmtMonthly)
                setIncType(runningDtMonthly)
                setExpType(runningDtMonthlyExp)
                setExpTotal(runningAmtMonthlyExp)
                

            }else{ //JWT is invalid or non-existent
                //setRecords([])
            }            

        })		
	}, [startDate, endDate, expStartDate, expEndDate])

	console.log(incType)
	console.log(expType)
	console.log()
	console.log(arrayTotal)
	console.log(incTotal)
	console.log(expTotal)

	return(
		<View>
			<h1 className="text-center ">Breakdown</h1>
			<Row>
				<Col md={6}>
					<h1 className="text-center">Income</h1>
					<div>
						<Form>
						  <Form.Row>
						    <Col>
						      <Form.Control type="date" placeholder="First name" onChange={e => setStartDate(e.target.value)}/>
						    </Col>
						    <Col>
						      <Form.Control type="date" placeholder="Last name" onChange={e => setEndDate(e.target.value)}/>
						    </Col>
						  </Form.Row>
						</Form>
					</div>
					<PieChart type={incType} total={incTotal} colors={bgColors}/>
				</Col>
				<Col md={6}>
					<h1 className="text-center">Expense</h1>
					<div>
						<Form>
						  <Form.Row>
						    <Col>
						      <Form.Control type="date" placeholder="First name" onChange={e => setExpStartDate(e.target.value)}/>
						    </Col>
						    <Col>
						      <Form.Control type="date" placeholder="Last name" onChange={e => setExpEndDate(e.target.value)}/>
						    </Col>
						  </Form.Row>
						</Form>
					</div>
					<PieChart type={expType} total={expTotal} colors={bgColors}/>
				</Col>
			</Row>
		</View>
	)
}