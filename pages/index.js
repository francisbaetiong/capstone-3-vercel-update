import React, { useContext } from 'react';
import Head from 'next/head';
import { Row, Col } from 'react-bootstrap';
import Login from  '../components/Login';
import View from '../components/View';
import UserContext from '../UserContext';
import styles from '../styles/Landing.module.css'

export default function Home() {

  const { user, setUser } = useContext(UserContext)

  console.log(user.id)

  return (
    <div className={styles.body}>
    {user.id !== null
      ?
      <div>     
        <Head>
          <title>Budget Tracker</title>
        </Head>
        <h1 className="text-center pt-5">Welcome</h1>
        <h5 className="text-center">Profile - Where you can see and update your profile details</h5>
        <h5 className="text-center">Categories - Create and edit existing categories for records creation</h5>
        <h5 className="text-center">Records - Create/update records and see list of records</h5>
        <h5 className="text-center">Trends - Create a line chart to represent balance</h5>
        <h5 className="text-center">Breakdown - See pie charts containing the breakdown of Income and Expense</h5>
      </div>
      :
      <View title={'Log In'}>
        <Row className="justify-content-center">
          <Col xs md="6">
            <Login/>
          </Col>
        </Row>
      </View>
    }
    </div>
  )
}
