import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import Router from 'next/router';
import { Table, Row, Container, Button, Modal, Form } from 'react-bootstrap';
import AppHelper from '../../app-helper';

export default function index(){
	const [records, setRecords] = useState([]);
	const [records2, setRecords2] = useState([]);

	const [income, setIncome] = useState('Income');
	const [expense, setExpense] = useState('Expense');

	const [smShow, setSmShow] = useState(false);
  	const [lgShow, setLgShow] = useState(false);

  	//modal useStates
  	const [categoryName, setCategoryName] = useState('');
	const [categoryType, setCategoryType] = useState('');
	const [categExists, setCategExists] = useState(true);
	const [isActive, setIsActive] = useState(false);

	//find index for updating
	const [indexInc, setIndexInc] = useState([]);
	const [indexExp, setIndexExp] = useState([]);

	//set index for income or exp
	const[firstIndex, setFirstIndex] = useState(0);

	//set length of details
	const [detLength, setDetLength] = useState(0);

	//sets if category is income or expense
	const [incOrExp, setIncOrExp] = useState('');

	//sets the category to be replaced
	const [prevIncCat, setPrevIncCat] = useState('');
	const [prevExpCat, setPrevExpCat] = useState('');

	//sets id of income or expense
	const [incOrExpId, setIncOrExpId] = useState('');

	//sets ids of details
	const [detailsId, setDetailsId] = useState([]);

	//delete modal
	const [show, setShow] = useState(false);
  	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);

  	//set category to be deleted
  	const [delCat, setDelCat] = useState('');
  	//set the ._id of the logged in user
  	const [loggedId, setLoggedId] = useState('');

  	const [incOrExp2, setIncOrExp2] = useState('');

	useEffect(() => {

        fetch(`${AppHelper.API_URL }/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
        	setLoggedId(data._id)

            if(data._id){ //JWT validated
                setRecords(data.income)
                setRecords2(data.expense)

                let inIndex = []
                data.income.map(res => {
                	inIndex.push(res.category)
                })
                setIndexInc(inIndex)

                let expIndex = []
                data.expense.map(res => {
                	expIndex.push(res.category)
                })
                setIndexExp(expIndex)

                let index1;
                let lengthOfDet;
                let idArray = []
                if((incOrExp !== '') && (prevIncCat !== '' || prevExpCat !== '')){
                	if(prevIncCat !== ''){
                		index1 = indexInc.indexOf(prevIncCat)
                		lengthOfDet = data.income[index1].details.length
                		setDetLength(lengthOfDet)
                		data.income[index1].details.map(getId => {
                			return idArray.push(getId._id)
                			
                		})
                		setDetailsId(idArray)
                	}else{
                		index1 = indexExp.indexOf(prevExpCat)
                		lengthOfDet = data.expense[index1].details.length
                		setDetLength(lengthOfDet)
                		data.expense[index1].details.map(getId => {
                			return idArray.push(getId._id)
                			
                		})
                		setDetailsId(idArray)
                	}
                }
                console.log(incOrExp)
                console.log(lengthOfDet)
                console.log(detLength)
                //console.log(data.incOrExp[firstIndex].details.length)

                if(categoryName !== ""){
                	setIsActive(true)
                }else{
                	setIsActive(false)
                }

            }else{ //JWT is invalid or non-existent
                setRecords([])
            }            

        })

    }, [incOrExp, prevIncCat, prevExpCat, detLength, categoryName])


	function editCategory(e){
		e.preventDefault()

		setCategoryName('')
		setIndexInc([])
		setIndexExp([])
		setCategExists(true)
		setIsActive(false)
		setIncOrExp('')
		setPrevIncCat('')
		setPrevExpCat('')
		setDetLength(0)
		setIncOrExpId('')
		setDetailsId([])
		setIsActive(false)

		fetch(`${AppHelper.API_URL}/users/editcategory`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				categoryName: categoryName,
				records: records,
				records2: records2,
				indexInc: indexInc,
				indexExp: indexExp,
				prevIncCat: prevIncCat,
				prevExpCat: prevExpCat,
				incOrExp: incOrExp,
				incOrExpId: incOrExpId,
				detailsId: detailsId

			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire('Category edited!', '', 'success')
				window.location.reload()				
			}else{
				Swal.fire('Something went wrong!', 'Please try again.', 'success')
			}
		})
	}

	function deleteCat(){

		setDelCat('')
		setIncOrExp2('')

		fetch(`${AppHelper.API_URL}/users/deletecategory`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				incOrExp2: incOrExp2,
				delCat: delCat,
				loggedId: loggedId

			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire('Category deleted!', '', 'success')
				window.location.reload()				
			}else{
				Swal.fire('Something went wrong!', 'Please try again.', 'success')
			}
		})

	}

	


	console.log(records2)
	console.log(indexInc)
	console.log(indexExp)
	console.log(prevIncCat)
	console.log(prevExpCat)
	console.log(incOrExp)
	console.log(categoryName)
	console.log(incOrExpId)
	console.log(detailsId)
	console.log(delCat)
	console.log(incOrExp2)



	return(
		<React.Fragment>
			<Container>
				<Row className="justify-content-md-center">
					<h1>Categories</h1>
					<Table striped bordered hover variant="secondary">
					  <thead>
					    <tr>
					      <th className="text-center">Category</th>
					      <th className="text-center">Type</th>
					      <th className="text-center">Actions</th>
					    </tr>
					  </thead>
					  <tbody>
					  {records.map(categories => {
					  	
					  	return(
					    <tr key={categories._id}>
					      <td className="text-center">{categories.category}</td>
					      <td className="text-center">{income}</td>
					      <td className="text-center">
						      <Button size="sm" variant="secondary" onClick={() => {setLgShow(true), setPrevIncCat(categories.category), setIncOrExp("income"), setIncOrExpId(categories._id)}}>Edit</Button>
						      <Modal
						        size="lg"
						        show={lgShow}
						        onHide={() => setLgShow(false)}
						        backdrop="static"
						        keyboard={false}
						        aria-labelledby="example-modal-sizes-title-lg"
						      >
						        <Modal.Header>
						          <Modal.Title id="example-modal-sizes-title-lg">
						            Edit Category
						          </Modal.Title>
						        </Modal.Header>
						        <Modal.Body>
						        	<Form onSubmit={e => editCategory(e)}>
									  <fieldset>

									    <Form.Group>
									      <Form.Label>Category Name</Form.Label>
									      <Form.Control onChange={e => {setCategoryName(e.target.value)}} placeholder="Enter updated category" />
									    </Form.Group>
									    {isActive === true
									    ?
									    <React.Fragment>
									    <Button type="submit">Submit</Button>
									    <Button variant="danger" href="/categories" onClick={() => {setDetLength(0), setPrevIncCat(''), setPrevExpCat(''), setIncOrExp(''), setDetailsId([]), setIncOrExpId('')}}>Cancel</Button>
									    </React.Fragment>
									    :
									    <React.Fragment>
									    <Button type="submit" disabled>Submit</Button>
									    <Button variant="danger" href="/categories" onClick={() => {setDetLength(0), setPrevIncCat(''), setPrevExpCat(''), setIncOrExp(''), setDetailsId([]), setIncOrExpId('')}}>Cancel</Button>
									    </React.Fragment>
										}
									  </fieldset>
									</Form>
						        </Modal.Body>
						      </Modal>
						      <Button className="ml-2" size="sm" variant="danger" onClick={() => {setDelCat(categories.category), setShow(true), setIncOrExp2("income")}}>Delete</Button>
						      <Modal show={show} onHide={handleClose} backdrop="static">
						        <Modal.Header>
						          <Modal.Title>Delete Category?</Modal.Title>
						        </Modal.Header>
						        <Modal.Body>Are you sure you want to delete this category?</Modal.Body>
						        <Modal.Footer>
						          <Button variant="secondary" onClick={() => {setDelCat(''), setShow(false), setIncOrExp2('')}}>
						            Close
						          </Button>
						          <Button variant="danger" onClick={() => deleteCat()}>
						            Delete
						          </Button>
						        </Modal.Footer>
						      </Modal>												      						    
					      </td>
					    </tr>
					  	)
					  })}
					  {records2.map(categories => {
					  	return(
					    <tr key={categories._id}>
					      <td className="text-center">{categories.category}</td>
					      <td className="text-center">{expense}</td>
					      <td className="text-center">
						      <Button size="sm" variant="secondary" onClick={() => {setLgShow(true), setPrevExpCat(categories.category), setIncOrExp("expense"), setIncOrExpId(categories._id)}}>Edit</Button>
						      <Modal
						        size="lg"
						        show={lgShow}
						        onHide={() => setLgShow(false)}
						        backdrop="static"
						        keyboard={false}
						        aria-labelledby="example-modal-sizes-title-lg"
						      >
						        <Modal.Header>
						          <Modal.Title id="example-modal-sizes-title-lg">
						            Edit Category
						          </Modal.Title>
						        </Modal.Header>
						        <Modal.Body>
						        	<Form onSubmit={e => editCategory(e)}>
									  <fieldset>

									    <Form.Group>
									      <Form.Label>Category Name</Form.Label>
									      <Form.Control onChange={e => {setCategoryName(e.target.value)}} placeholder="Enter updated category" />
									    </Form.Group>
									    {isActive === true
									    ?
									    <React.Fragment>
									    <Button type="submit">Submit</Button>
									    <Button variant="danger" href="/categories" onClick={() => {setDetLength(0), setPrevIncCat(''), setPrevExpCat(''), setIncOrExp(''), setDetailsId([]), setIncOrExpId('')}}>Cancel</Button>
									    </React.Fragment>
									    :
									    <React.Fragment>
									    <Button type="submit" disabled>Submit</Button>
									    <Button variant="danger" href="/categories" onClick={() => {setDetLength(0), setPrevIncCat(''), setPrevExpCat(''), setIncOrExp(''), setDetailsId([]), setIncOrExpId('')}}>Cancel</Button>
									    </React.Fragment>
										}
									  </fieldset>
									</Form>
						        </Modal.Body>
						      </Modal>
						      <Button className="ml-2" size="sm" variant="danger" onClick={() => {setDelCat(categories.category), setShow(true), setIncOrExp2("expense")}}>Delete</Button>
						      <Modal show={show} onHide={handleClose} backdrop="static">
						        <Modal.Header>
						          <Modal.Title>Delete Category?</Modal.Title>
						        </Modal.Header>
						        <Modal.Body>Are you sure you want to delete this category?</Modal.Body>
						        <Modal.Footer>
						          <Button variant="secondary" onClick={() => {setDelCat(''), setShow(false), setIncOrExp2('')}}>
						            Close
						          </Button>
						          <Button variant="danger" onClick={() => deleteCat()}>
						            Delete
						          </Button>
						        </Modal.Footer>
						      </Modal>						    
					      </td>
					    </tr>
					  	)
					  })}
					  </tbody>
					</Table>

				</Row>
				<div className="text-center">
					<Button variant="primary" href="/categories/new">
						Create Category
					</Button>
					</div>	
			</Container>
		</React.Fragment>
	)
}

