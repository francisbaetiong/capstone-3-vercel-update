import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router from 'next/router';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';

export default function newCategory(){

	const [categoryName, setCategoryName] = useState('');
	const [categoryType, setCategoryType] = useState('');
	const [categExists, setCategExists] = useState(true);
	const [isActive, setIsActive] = useState(false);

	const { user, setUser } = useContext(UserContext)

	useEffect(() => {
		if((categoryName !== "") && (categoryType === "Income" || categoryType === "Expense")){
			
			fetch(`${AppHelper.API_URL}/users/category-exists`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					userId: user.id,
					category: categoryName,
					categoryType: categoryType
				})
			})
			.then(res => res.json())
			.then(data => {
				if((data === false && categoryName !== "") && (categoryType === "Income" || categoryType === "Expense")){
					setCategExists(false);
					setIsActive(true)
				}else{
					Swal.fire('Category already exists!', 'Please enter a non-existing category', 'error')
					setCategExists(true);
					setIsActive(false);
				}
			})						
		}else{
			setIsActive(false)
		}
	}, [categoryName, categoryType, isActive])

	console.log(categoryName)
	console.log(categoryType)

	function addCategory(e){
		e.preventDefault()

		setCategoryName('')
		setCategoryType('')
		setCategExists(true)
		setIsActive(false)

		fetch(`${AppHelper.API_URL}/users/addcategory`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${ AppHelper.getAccessToken() }`
			},
			body: JSON.stringify({
				category: categoryName,
				categoryType: categoryType
			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			if(data === true){
				Swal.fire('Category added!', '', 'success')
				Router.push('/categories')				
			}else{
				Swal.fire('Something went wrong!', 'Please try again.', 'success')
			}
		})
	}

	return(
		<View title={'Create Category'}>
		<Card>
			<Card.Header>Create Category</Card.Header>
			<Card.Body>
				<Form onSubmit={e => addCategory(e)}>
				  <fieldset>
				    <Form.Group>
				      <Form.Label>Category Name</Form.Label>
				      <Form.Control onChange={e => setCategoryName(e.target.value)} placeholder="Input category name" />
				    </Form.Group>
				    <Form.Group>
				      <Form.Label>Category Type</Form.Label>
				      <Form.Control as="select" onChange={e => setCategoryType(e.target.value)}>
				      	<option></option>
				        <option>Income</option>
				        <option>Expense</option>
				      </Form.Control>
				    </Form.Group>
				    {isActive === true
				    ?
				    <Button type="submit">Submit</Button>
				    :
				    <Button type="submit" disabled>Submit</Button>
					}
					<Button variant="secondary" href="/categories" onClick={() => {setCategoryName(''), setCategoryType('')}}>Cancel</Button>
				  </fieldset>
				</Form>
			</Card.Body>
		</Card>
		</View>
	)
}