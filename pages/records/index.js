import React, { useEffect, useState, useContext } from 'react';
import { Card, Row, Col, InputGroup, Button, FormControl, DropdownButton, Dropdown, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import moment from 'moment';
import View from '../../components/View';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';

export default function index() {

	const [income, setIncome] = useState([]);
	const [expense, setExpense] = useState([]);
	const [bothTypes, setBothTypes] = useState([]);

	const [incomeCat, setIncomeCat] = useState([]);
	const [total, setTotal] = useState([]);

	const [search, setSearch] = useState('');

	const [dropSearch, setDropSearch] = useState('');

	//edit records modal
	const [smShow, setSmShow] = useState(false);
  	const [lgShow, setLgShow] = useState(false);

  	//for button rendering
  	const [isActive, setIsActive] = useState(false);

  	//set if income or expense
	const [incOrExp, setIncOrExp] = useState('');


  	//array for income and expense
  	const [indexInc, setIndexInc] = useState([]);
	const [indexExp, setIndexExp] = useState([]);

  	//sets new inputs for edit records
  	const [updatedDesc, setUpdatedDesc] = useState('');
  	const [updatedAmt, setUpdatedAmt] = useState('');
  	const [updatedDate, setUpdatedDate] = useState('');

  	//set the category for index setup
  	const [category, setCategory] = useState('');

  	const [detailId, setDetailId] = useState('');

  	//delete modal
	const [show, setShow] = useState(false);
  	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);

  	//sets id to be deleted
  	const [delRec, setDelRec] = useState('');

  	//sets income or expense id for deletion
	const [incOrExp2, setIncOrExp2] = useState('');

	//sets the income type id
	const [incOrExpTypeId, setIncOrExpTypeId] = useState('');


	const [balance, setBalance] = useState(0);

	const { user, setUser } = useContext(UserContext)

	useEffect(() => {

		fetch(`${AppHelper.API_URL }/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
        	console.log(data)
            if(data._id){ //JWT validated
                setIncome(data.income)
                setExpense(data.expense)

                 let inIndex = []
                data.income.map(res => {
                	inIndex.push(res.category)
                })
                setIndexInc(inIndex)

                let expIndex = []
                data.expense.map(res => {
                	expIndex.push(res.category)
                })
                setIndexExp(expIndex)               

                setBothTypes(data.income.concat(data.expense))
                let arrayIncome = []
                for(let i = 0; i < data.income.length; i++){
	                data.income[i].details.map(res => {
	                	return arrayIncome.push(res)
	                })
            	}
                for(let i = 0; i < data.expense.length; i++){
	                data.expense[i].details.map(res => {
	                	return arrayIncome.push(res)
	                })
            	}
            	let sortedArray = arrayIncome.sort(function(a,b){
				  // Turn your strings into dates, and then subtract them
				  // to get a value that is either negative, positive, or zero.
				  return new Date(b.dateMade) - new Date(a.dateMade);
				});           	
                setIncomeCat(sortedArray)


                                
                if(dropSearch === "Income") {
                	let filteredArray = sortedArray.filter(f => f.classification.includes(dropSearch))
                	setIncomeCat(filteredArray)

                	if(search !== "") {
                	let filteredArray2 = filteredArray.filter(f => f.description.toLowerCase().includes(search))
                		setIncomeCat(filteredArray2)
	                }

                }else if(dropSearch === "Expense"){
                	let filteredArray = sortedArray.filter(f => f.classification.includes(dropSearch))
                	setIncomeCat(filteredArray)

                	if(search !== "") {
                	let filteredArray2 = filteredArray.filter(f => f.description.toLowerCase().includes(search))
                		setIncomeCat(filteredArray2)
	                }

                	//setIncomeCat(sortedArray)
                }else{
                	if(search !== "") {
                	let filteredArray = sortedArray.filter(f => f.description.toLowerCase().includes(search))
                		setIncomeCat(filteredArray)
	                }
                }

                // if(search !== "") {
                // 	let filteredArray = sortedArray.filter(f => f.description.toLowerCase().includes(search))
                // 	setIncomeCat(filteredArray)
                // }else{
                // 	setIncomeCat(sortedArray)
                // }


                let runningAmt = []
                let runningAmt2 = []

                sortedArray.forEach(run => {
                	if(run.classification === "Income"){
                		runningAmt2.push(runningAmt += parseInt(run.amount))
                	}else{
                		runningAmt2.push(runningAmt -= parseInt(run.amount))
                	}
                })

                setTotal(runningAmt)

                if(updatedDesc !== "" && updatedAmt !== "" && updatedDate !== ""){
                	setIsActive(true)
                }else{
                	setIsActive(false)
                }

                if(category !== "" && incOrExp2 !== ""){
                	if(incOrExp2 === "income"){
                		data.income.forEach(res => {
                			if(res.category === category){
                				setIncOrExpTypeId(res._id)
                			}
                		})
                	}else{
                		data.expense.forEach(res => {
                			if(res.category === category){
                				setIncOrExpTypeId(res._id)
                			}
                		})
                	}
                }

            }else{ //JWT is invalid or non-existent
                 setIncome([])
                 setExpense([])
            }            

        })

	}, [search, dropSearch, updatedDesc, updatedAmt, updatedDate, category, incOrExp2])


	function editRecords(e){
		e.preventDefault()

		setIncOrExp('')
		setUpdatedDesc('')
		setUpdatedAmt('')
		setUpdatedDate('')
		setDetailId('')
		setCategory('')
		setIsActive(false);

		fetch(`${AppHelper.API_URL}/users/editrecords`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				updatedDesc: updatedDesc,
				updatedAmt: updatedAmt,
				updatedDate: updatedDate,
				indexInc: indexInc,
				indexExp: indexExp,
				incOrExp: incOrExp,
				category: category,
				detailId: detailId

			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire('Record updated!', '', 'success')
				window.location.reload()				
			}else{
				Swal.fire('Something went wrong!', 'Please try again.', 'success')
			}
		})


	}

	function deleteRec(){

		setDelRec('')
		setIncOrExp2('')
		setIncOrExpTypeId('')
		setCategory('')

		fetch(`${AppHelper.API_URL}/users/deleterecord`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				incOrExp2: incOrExp2,
				delRec: delRec,
				incOrExpTypeId: incOrExpTypeId,
				category: category,
				indexInc: indexInc,
				indexExp: indexExp

			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire('Record deleted!', '', 'success')
				window.location.reload()				
			}else{
				Swal.fire('Something went wrong!', 'Please try again.', 'success')
			}
		})

	}

	//console.log(income)
	//console.log(expense)
	console.log(bothTypes)
	console.log(incomeCat)
	console.log(dropSearch)
	console.log(incOrExp)
	console.log(incOrExpTypeId)
	console.log(balance)



	return(
		<View title={'Records'}>
			<h1>Records</h1>
		  	<InputGroup className="mb-3">
			    <InputGroup.Prepend>
			      	<Button variant="primary" href="/records/newRecord">Add</Button>
			    </InputGroup.Prepend>
			    <FormControl aria-describedby="basic-addon1" onChange={e => setSearch(e.target.value)} />
				<Form.Control as="select" onChange={e => setDropSearch(e.target.value)}>
			      <option>All</option>
			      <option>Income</option>
			      <option>Expense</option>
			    </Form.Control>
		  	</InputGroup>
		  	{incomeCat.map(element => {
		  		
			return(
			<Card className="pt-2 pb-2" key={element._id}>
				<Row>
					<Col><h2 className="ml-3">{element.description}</h2></Col>
					{element.classification === "Income"
						?
						<Col xs md="2"><h2 className="text-center">{element.amount}</h2></Col>
						:
						<Col xs md="2"><h2 className="text-center">-{element.amount}</h2></Col>
					}
				</Row>
				<Row>
					<Col><h4 className="ml-3">{element.classification}({element.category})</h4></Col>
					<Col xs md="2"><h2 className="text-center"></h2></Col>
				</Row>
				<Row>
					<Col><h5 className="ml-3">{moment(element.dateMade).format("MMM DD YYYY")}</h5></Col>
				</Row>
				<div className="text-center">
					<Button size="sm" variant="secondary" onClick={() => {setLgShow(true), setIncOrExp(element.classification.toLowerCase()), setCategory(element.category), setDetailId(element._id)}}>Edit</Button>
			      <Modal
			        size="lg"
			        show={lgShow}
			        onHide={() => setLgShow(false)}
			        backdrop="static"
			        keyboard={false}
			        aria-labelledby="example-modal-sizes-title-lg"
			      >
			        <Modal.Header>
			          <Modal.Title id="example-modal-sizes-title-lg">
			            Edit Category
			          </Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			        	<Form onSubmit={e => editRecords(e)}>
						  <fieldset>

						    <Form.Group>
						      <Form.Label>Description</Form.Label>
						      <Form.Control type="text" onChange={e => {setUpdatedDesc(e.target.value)}} placeholder="Enter updated description" required />
						    </Form.Group>
						    <Form.Group>
						      <Form.Label>Amount</Form.Label>
						      <Form.Control type="number" onChange={e => {setUpdatedAmt(e.target.value)}} placeholder="Enter updated amount" required />
						    </Form.Group>
						    <Form.Group>
						      <Form.Label>Date</Form.Label>
						      <Form.Control type="date" onChange={e => {setUpdatedDate(e.target.value)}} placeholder="Enter updated date" required />
						    </Form.Group>
						    {isActive === true
						    ?
						    <React.Fragment>
						    <Button type="submit">Submit</Button>
						    <Button variant="danger" href="/records" onClick={() => {setIncOrExp(''), setUpdatedDesc(''), setUpdatedAmt(''), setUpdatedDate(''), setDetailId(''), setCategory('')}}>Cancel</Button>
						    </React.Fragment>
						    :
						    <React.Fragment>
						    <Button type="submit" disabled>Submit</Button>
						    <Button variant="danger" href="/records" onClick={() => {setIncOrExp(''), setUpdatedDesc(''), setUpdatedAmt(''), setUpdatedDate(''), setDetailId(''), setCategory('')}}>Cancel</Button>
						    </React.Fragment>
							}
						  </fieldset>
						</Form>
			        </Modal.Body>
			      </Modal>
      			  <Button className="ml-2" size="sm" variant="danger" onClick={() => {setDelRec(element._id), setShow(true), setIncOrExp2(element.classification.toLowerCase(), setCategory(element.category))}}>Delete</Button>
			      <Modal show={show} onHide={handleClose} backdrop="static">
			        <Modal.Header>
			          <Modal.Title>Delete Record?</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>Are you sure you want to delete this record?</Modal.Body>
			        <Modal.Footer>
			          <Button variant="secondary" onClick={() => {setDelRec(''), setShow(false), setIncOrExp2(''), setCategory(''), setIncOrExpTypeId('')}}>
			            Close
			          </Button>
			          <Button variant="danger" onClick={() => deleteRec()}>
			            Delete
			          </Button>
			        </Modal.Footer>
			      </Modal>								
				</div>								
			</Card>
			)
			})}
			<Row className="justify-content-center">
				<Col xs={6}>
					<h2 className="text-center ">Running Amount</h2>
					<Card className="text-center p-3">
						<h1>{total}</h1>
					</Card>	
				</Col>
			</Row>
		</View>
	)
}