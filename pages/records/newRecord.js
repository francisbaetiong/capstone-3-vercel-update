import { useState, useEffect, useContext } from 'react';
import { Card, Form, Button } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';

export default function newRecords(){

	//type of income or expense
	const [category, setCategory] = useState('');
	//income or expense
	const [categoryType, setCategoryType] = useState('');
	//stores data.income or data.expense
	const [incomeCategory, setIncomeCategory] = useState([]);
	const [amount, setAmount] = useState('');
	const [description, setDescription] = useState('');
	const [dateMade, setDateMade] = useState('');

	//stores index of category selected
	const [index, setIndex] = useState(0);

	const [isActive, setIsActive] = useState(false);

	const { user, setUser } = useContext(UserContext)

	useEffect(() => {

        fetch(`${AppHelper.API_URL }/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
        	console.log(data)
            if(data._id){ //JWT validated

                if(categoryType === "Income"){
                	setIncomeCategory(data.income)
                	let pos = data.income.map(function(e) { return e.category; }).indexOf(category);
                	setIndex(pos)

                }else if(categoryType === "Expense"){
                	setIncomeCategory(data.expense)
                	let pos2 = data.expense.map(function(e) { return e.category; }).indexOf(category);
                	setIndex(pos2)
                }

            }else{ //JWT is invalid or non-existent
                //setRecords([])
            }            

        })
	}, [categoryType, category, amount, description])

	useEffect(() => {
		if(categoryType !== '' && category !== '' && amount !== '' && description !== '' && dateMade !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [categoryType, category, amount, description, dateMade])

	function addRecord(e){
		e.preventDefault()

		setCategory('');
		setCategoryType('');
		setIncomeCategory([]);
		setAmount('');
		setDescription('');
		setIndex(0);
		setDateMade('');

		fetch(`${AppHelper.API_URL}/users/addrecords`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${ AppHelper.getAccessToken() }`
			},
			body: JSON.stringify({
				index: index,
				categoryType: categoryType,
				amount: amount,
				dateMade: dateMade,
				description: description,
				category: category
			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			if(data === true){
				Swal.fire('Record added!', '', 'success')
				Router.push('/records')				
			}else{
				Swal.fire('Something went wrong!', 'Please try again.', 'error')
			}
		})		

	}

	// console.log(categoryType)
	 console.log(incomeCategory)
	// console.log(amount)
	console.log(description)
	console.log(index)
	console.log(isActive)

	return(
		<View title={'Create Record'}>
			<Card>
				<Card.Header>Create Record</Card.Header>
				<Card.Body>
					<Form onSubmit={e => addRecord(e)}>
					  <fieldset>
					    <Form.Group>
					      <Form.Label>Category Type</Form.Label>
					      <Form.Control as="select" onChange={e => setCategoryType(e.target.value)}>
					      	<option></option>
					        <option>Income</option>
					        <option>Expense</option>
					      </Form.Control>
					    </Form.Group>
					    <Form.Group>
					      <Form.Label>Category Name</Form.Label>
					      <Form.Control as="select" onChange={e => setCategory(e.target.value)}>
					      	<option></option>
					      	{incomeCategory.map(element => {
					      		return (
					      			<option key={element._id}>{element.category}</option>
					      		)
					      	})}
					        
					      </Form.Control>
					    </Form.Group>					    
					    <Form.Group>
					      <Form.Label>Amount:</Form.Label>
					      <Form.Control type="number" onChange={e => setAmount(e.target.value)} placeholder="Input amount" />
					    </Form.Group>
					    <Form.Group>
					      <Form.Label>Description:</Form.Label>
					      <Form.Control type="text" onChange={e => setDescription(e.target.value)} placeholder="Input description" />
					    </Form.Group>
					    <Form.Group>
					      <Form.Label>Date:</Form.Label>
					      <Form.Control type="date" onChange={e => setDateMade(e.target.value)} placeholder="Input date" />
					    </Form.Group>
					    {isActive === true
					    ?
					    <Button type="submit">Submit</Button>
					    :
					    <Button type="submit" disabled>Submit</Button>
						}
						<Button variant="secondary" href="/records" onClick={() => {setAmount(''), setDescription(''), setDateMade('')}}>Cancel</Button>
					  </fieldset>
					</Form>
				</Card.Body>
			</Card>
		</View>
	)
}