import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Container } from 'react-bootstrap'
import Router from 'next/router';
import Head from 'next/head'
import Swal from 'sweetalert2';
import View from '../../components/View';

import AppHelper from '../../app-helper';

export default function index(){
	const [email, setEmail] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	//state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false)

	function registerUser(e){
		e.preventDefault()

		console.log(`${email} is registered with password ${password1}`)

		setEmail('')
		setFirstName('')
		setLastName('')
		setPassword1('')
		setPassword2('')

		fetch(`${AppHelper.API_URL}/users/`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				loginType: "email"
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire('Successfully Registered!', 'Please login to continue', 'success')
				Router.push('/')
			}else{
				Swal.fire('Registration failed!', 'Please try again', 'error')
			}
		})

	}

	useEffect(() => {
		if(email !== ''){
			

			fetch(`${AppHelper.API_URL}/users/email-exists`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data === false){
					setIsActive(true)

				}else{
					Swal.fire('Email taken!', 'Please use a different email to register', 'error')
				}
			})

		}else{
			setIsActive(false)
		}
	}, [email, firstName, lastName, password1, password2])

	return(

	<React.Fragment>
	<View title={'Register'}>

		<Container>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
					<Form.Text className="text-muted">We'll never share your password with anyone else</Form.Text>
				</Form.Group>
				<Form.Group controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Email" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Email" value={lastName} onChange={e => setLastName(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="password2">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
				</Form.Group>

			{/*Conditionally render submit button based on isActive state*/}
				{((isActive === true && email !== '' && firstName !== '' && lastName !== '' && password1 !== '' && password2 !== '') && (password1 === password2))
					? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
					: <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
				}

			</Form>
		</Container>
	</View>
	</React.Fragment>
	)
}