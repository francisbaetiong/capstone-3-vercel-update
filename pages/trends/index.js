import { useState, useEffect, useContext} from 'react';
import React from 'react';
import { Form, Col, Row } from 'react-bootstrap';
import moment from 'moment';

import View from '../../components/View';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';

import LineChart from '../../components/LineChart'

export default function index(){
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	const [lineData, setLineData] = useState('');
	const [arrayTotal, setArrayTotal] = useState([]);

	const [runTotal, setRunTotal] = useState(0);
	const [months, setMonths] = useState(0);

	const [totalTest, setTotalTest] = useState(0);

	const {user, setUser} = useContext(UserContext);

	useEffect(() => {
 
        fetch(`${AppHelper.API_URL }/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
        	console.log(data)
            if(data._id){ //JWT validated

                let arrayData = []
                for(let i = 0; i < data.income.length; i++){
	                data.income[i].details.map(res => {
	                	return arrayData.push(res)
	                })
            	}
                for(let i = 0; i < data.expense.length; i++){
	                data.expense[i].details.map(res => {
	                	return arrayData.push(res)
	                })
            	}
            	let sortedArray = arrayData.sort(function(a,b){
				  // Turn your strings into dates, and then subtract them
				  // to get a value that is either negative, positive, or zero.
				  return new Date(a.dateMade) - new Date(b.dateMade);
				});           	
                setArrayTotal(sortedArray)

                let runningAmt = []
                let runningDt = []
                let totalAmt = 0

                let runningAmtMonthly = []
                let runningDtMonthly = []

                sortedArray.map(run => {
                	if((moment(run.dateMade).format("YYYY-MM-DD") >= startDate && moment(run.dateMade).format("YYYY-MM-DD") <= moment(run.dateMade).format("YYYY-MM-DD")) && moment(run.dateMade).format("YYYY-MM-DD") <= endDate){
	                	
 	                	if(!runningDtMonthly.includes(moment(run.dateMade).format("MMM YYYY"))){
 	                		if(runningAmtMonthly.length > 0){
 	                		runningAmtMonthly.push((run.classification === "Income" ? parseInt(run.amount) : parseInt(run.amount) * -1) + totalAmt)
		                	runningDtMonthly.push(moment(run.dateMade).format("MMM YYYY"))
		                	 	   totalAmt = run.classification === "Income" ? totalAmt + parseInt(run.amount) : totalAmt - parseInt(run.amount)             			
 	                		}else{
 	                		runningAmtMonthly.push(parseInt(run.amount))
		                	runningDtMonthly.push(moment(run.dateMade).format("MMM YYYY"))
		                	totalAmt = totalAmt + parseInt(run.amount)
		                	}
		                }else{
		                	totalAmt = run.classification === "Income" ? totalAmt + parseInt(run.amount) : totalAmt - parseInt(run.amount)
		                	runningAmtMonthly[runningDtMonthly.indexOf(moment(run.dateMade).format("MMM YYYY"))] = run.classification === "Income" 
		                	? runningAmtMonthly[runningDtMonthly.indexOf(moment(run.dateMade).format("MMM YYYY"))] + parseInt(run.amount)
		                	: runningAmtMonthly[runningDtMonthly.indexOf(moment(run.dateMade).format("MMM YYYY"))] - parseInt(run.amount)
		                }	                  		

	                	// if(run.classification === "Income"){
	                	// 	totalAmt = totalAmt + parseInt(run.amount)
	                	// 	runningAmt.push(totalAmt)
             		
	                	// }else{
	                	// 	totalAmt = totalAmt - parseInt(run.amount)
	                	// 	runningAmt.push(totalAmt)
	                	// }
                	}
                	
                })
                setRunTotal(runningAmtMonthly)
                setMonths(runningDtMonthly)
                setTotalTest(totalAmt)
                

            }else{ //JWT is invalid or non-existent
                //setRecords([])
            }            

        })		
	}, [startDate, endDate])

	console.log(months)
	console.log(arrayTotal)
	console.log(runTotal)
	console.log(totalTest)

	return(
		<View title={'Trends'}>
			<h1>Balance Trend</h1>
			<div>
				<Form>
				  <Form.Row>
				    <Col>
				      <Form.Control type="date" placeholder="First name" onChange={e => setStartDate(e.target.value)}/>
				    </Col>
				    <Col>
				      <Form.Control type="date" placeholder="Last name" onChange={e => setEndDate(e.target.value)}/>
				    </Col>
				  </Form.Row>
				</Form>
			</div>
			<LineChart runTotal={runTotal} months={months} />
		</View>

	)
}