
import { useState, useEffect, useContext } from 'react';
import { Card, Row, Col, InputGroup, Button, FormControl, DropdownButton, Dropdown, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router from 'next/router';
import View from '../../components/View';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';



export default function index(){

	const [field, setField] = useState(false);
	const [listFirst, setListFirst] = useState('');
	const [listLast, setListLast] = useState('');
	const [listMail, setListMail] = useState('');
	const [editFirst, setEditFirst] = useState('');
	const [editLast, setEditLast] = useState('');
	const [editMail, setEditMail] = useState('');
	const [isActive, setIsActive] = useState(false);

	const[currentMail, setCurrentMail] = useState('');

	const [loginType, setLoginType] = useState('');
	
	const { user, setUser } = useContext(UserContext)

	console.log(user.id)

	useEffect(() => {
	    fetch(`${AppHelper.API_URL }/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        })
        .then(res => res.json())
        .then(data => {
        	console.log(data)
        	setLoginType(data.loginType)
            if(data._id){ //JWT validated

            	setListFirst(data.firstName)
            	setListLast(data.lastName)
            	setListMail(data.email)
            	setCurrentMail(data.email)

            }else{ //JWT is invalid or non-existent
                //setRecords([])
            }            

        })
	}, [])

	useEffect(() => {
		if(editMail !== ''){
			

			fetch(`${AppHelper.API_URL}/users/email-exists2`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: editMail,
					currentMail: currentMail
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data === false){
					setIsActive(true)

				}else{
					Swal.fire('Email taken!', 'Please use a different email to register', 'error')
					setIsActive(false)
				}
			})

		}else{
			setIsActive(false)
		}
	}, [editMail])


	function editProfile(e){
		e.preventDefault()

		setEditFirst('');
		setEditLast('');
		setEditMail('');
		setField('')
		setIsActive(false)

		fetch(`${AppHelper.API_URL}/users/editprofile`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${ AppHelper.getAccessToken() }`
			},
			body: JSON.stringify({
				userId: user.id,
				firstName: editFirst,
				lastName: editLast,
				email: editMail

			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			if(data === true){
				Swal.fire('Profile updated!', '', 'success')
				// setEditFirst('')
				// setEditLast('')
				// setEditMail('')
				// setField('')
				// setIsActive(false)
				window.location.reload()				
			}else{
				Swal.fire('Something went wrong!', 'Please try again.', 'error')
			}
		})		

	}
	

	return(
		<View title={'Profile'}>
			<Card>
				<Card.Header className="text-center">
					<h1>Profile</h1>
				</Card.Header>
				<Card.Body>
					<Form onSubmit={e => editProfile(e)}>
						<Form.Group as={Row}>
							<Form.Label column sm="6" className="text-center">First Name:</Form.Label>
							<Col sm="6">
								{field === false
								?
								<Form.Control className="text-center" type="text" placeholder={listFirst} disabled/>
								:
								<Form.Control type="text" placeholder={listFirst} onChange={e => setEditFirst(e.target.value)} required/>
								}
							</Col>
						</Form.Group>
						<Form.Group as={Row}>
							<Form.Label column sm="6" className="text-center">Last Name:</Form.Label>
							<Col sm="6">
								{field === false
								?
								<Form.Control className="text-center" type="text" placeholder={listLast} disabled/>
								:
								<Form.Control type="text" placeholder={listLast} onChange={e => setEditLast(e.target.value)} required/>
								}
							</Col>
						</Form.Group>
						<Form.Group as={Row}>
							<Form.Label column sm="6" className="text-center">Email:</Form.Label>
							<Col sm="6">
								{field === false
								?
								<Form.Control className="text-center" type="text" placeholder={listMail} disabled/>
								:
								<Form.Control type="email" placeholder={listMail} onChange={e => setEditMail(e.target.value)} required/>
								}
							</Col>
						</Form.Group>
						<div className="text-center" >
							{loginType === "google"
							? 
								<div>
								<Button variant="secondary" disabled>Edit Profile</Button>
							  	<h5>Register with mail to edit profile</h5>
							  	</div>
							:
								field === false
								?
								<Button className="text-center" variant="secondary" onClick={() => setField(true)}>Edit Profile</Button>
								:
								<Button className="text-center" variant="danger" onClick={() => setField(false)} href="/profile">Cancel</Button>
								
							}
						</div>
						{field === false
						?
						<div></div>
						:
						(isActive === true && editFirst !== '' && editLast !== '')
							?
							<div className="text-center mt-2" >
								<Button className="text-center" variant="primary" type="submit">Submit</Button>
							</div>
							:
							<div className="text-center mt-2" >
								<Button className="text-center" variant="primary" type="submit" disabled>Submit</Button>
							</div>
						}
					</Form>

				</Card.Body>
			</Card>
		</View>
	)

}