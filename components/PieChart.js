import React from 'react';
import {Pie} from 'react-chartjs-2';


export default function PieChart({ type, total, colors }){

	const data = {
		labels: type,
		datasets: [{
			data: total,
			backgroundColor: colors,
			hoverBackgroundColor: colors
		}]
	};

	return(
		<div>
	        <Pie data={data} />
      	</div>
	)

}