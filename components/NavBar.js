import React, { useContext } from 'react';
import Link from 'next/link';
import { Navbar, Nav } from 'react-bootstrap';

import UserContext from '../UserContext'

export default function NavBar(){

	const { user } = useContext(UserContext)

	return(
		<Navbar bg="dark" expand="lg" variant="dark" className="navfont">
	    	<Link href={user.id === null ? "/" : "/"}>
	    		<a className="navbar-brand">Budget Tracker</a>
	    	</Link>
	      <Navbar.Toggle aria-controls="basic-navbar-nav"/>
	      <Navbar.Collapse id="basic-navbar-nav">
	        <Nav className="mr-auto">

	          	{(user.id !== null)
	          		? (user.isAdmin === true)
	          			?
	          			<React.Fragment>
	          				<Link href="/profile">
	          					<a className="nav-link" role="button">Profile</a>
	          				</Link>
	          				<Link href="/categories">
		          					<a className="nav-link" role="button">Categories</a>
		          			</Link>
		          			<Link href="/records">
		          					<a className="nav-link" role="button">Records</a>
		          			</Link>
		          			<Link href="/trends">
		          					<a className="nav-link" role="button">Trends</a>
		          			</Link>
		          			<Link href="/breakdown">
		          					<a className="nav-link" role="button">Breakdown</a>
		          			</Link>		          					          			
	          				<Link href="/logout">
	          					<a className="nav-link" role="button">Log Out</a>
	          				</Link>
	          			</React.Fragment>
	          			:
	          			<React.Fragment>
	          				<Link href="/profile">
	          					<a className="nav-link" role="button">Profile</a>
	          				</Link>
		          			<Link href="/categories">
		          					<a className="nav-link" role="button">Categories</a>
		          			</Link>
		          			<Link href="/records">
		          					<a className="nav-link" role="button">Records</a>
		          			</Link>
		          			<Link href="/trends">
		          					<a className="nav-link" role="button">Trends</a>
		          			</Link>
		          			<Link href="/breakdown">
		          					<a className="nav-link" role="button">Breakdown</a>
		          			</Link>		          					          			
		          			<Link href="/logout">
		          					<a className="nav-link" role="button">Log Out</a>
		          			</Link>
		          		</React.Fragment>
	          		:
	          			<React.Fragment>
	          				<Link href="/register">
	          					<a className="nav-link" role="button">Register</a>
	          				</Link>
	          			</React.Fragment>

	          	}

	        </Nav>
	      </Navbar.Collapse>
	    </Navbar>
	)
}