import React, { useState, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import Router from 'next/router';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';

import AppHelper from '../app-helper';
import UserContext from '../UserContext';

export default function Login(){
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [willRedirect, setWillRedirect] = useState(false)
	//consume the UserContext object to get the values that are passed by it
	const { user, setUser } = useContext(UserContext)



	function authenticate(e){
		e.preventDefault()

		const options= {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}

		fetch(`${AppHelper.API_URL}/users/login`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)

			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
			}else{
				if(data.error === 'does-not-exist'){
					Swal.fire('Authentication failed', 'User does not exist', 'error')
				}else if(data.error === 'incorrect-password'){
					Swal.fire('Authentication failed', 'Password is incorrect', 'error')
				}else if(data.error === 'login-type-error'){
					Swal.fire('Login Type Error', 'You may have registered through a different login method, try alternative login precedures.', 'error')
				}
			}
		})
	}

	function authenticateGoogleToken(response){
		//console.log(response)
		const payload = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				tokenId: response.tokenId
			})
		}

		fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
			}else{
				if(data.error === 'google-auth-error'){
					Swal.fire('Google Auth Error', 'Google authentication procedure failed', 'error')
				}else if(data.error === 'login-type-error'){
					Swal.fire('Login Type Error', 'You may have registered through a different login method', 'error')
				}
			}
		})
	}

	function retrieveUserDetails(accessToken){
		const options = {
			headers: { Authorization: `Bearer ${accessToken}`}
		}
		fetch(`${AppHelper.API_URL}/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

			Router.push('/categories')
		})

		console.log(accessToken)
	}


	return(
		<React.Fragment>
		<h3>Log In</h3>
		<Card>
			<Card.Header>Login Details</Card.Header>
			<Card.Body>
				<Form onSubmit={e => authenticate(e)}>
					<Form.Group>
							<Form.Label>Email Address</Form.Label>
							<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>
					<Form.Group>
							<Form.Label>Password</Form.Label>
							<Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
					</Form.Group>

					<Button variant="primary" type="submit" block>Submit</Button>
					<GoogleLogin 
					clientId="360458903242-a55ovoqr5osgmokm45isjm4rm6ifvi47.apps.googleusercontent.com"
					buttonText="Login"
					onSuccess={ authenticateGoogleToken }
					onFailure={ authenticateGoogleToken }
					cookiePolicy={ 'single_host_origin'}
					className="w-100 text-center d-flex justify-content-center"
					/>
				</Form>
			</Card.Body>
		</Card>
		</React.Fragment>
		
	)
}