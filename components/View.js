import React from 'react';
import Head from 'next/head';
import { Container } from 'react-bootstrap';

export default function View({title, children}){
	//"children" parameter is a reserved keyword that containers the child/sub components of this component
	return(
		<React.Fragment>
			<Head>
				<title key="title-tag">{title}</title>
				<meta key="title-meta" name="viewport" content="initial-scale=1.0, width=device-width"/>
			</Head>
			<Container className="pt-4 mb-5">
				{ children }
			</Container>
		</React.Fragment>
	)
}