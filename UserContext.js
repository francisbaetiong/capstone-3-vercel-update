//import React
import React from 'react';

//create a context object
const UserContext = React.createContext();

//create a provider component that allows using our contexts
export const UserProvider = UserContext.Provider;

//export UserContext
export default UserContext;
