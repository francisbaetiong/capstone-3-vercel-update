module.exports = {
	API_URL: "https://francis-capstone-3.herokuapp.com/api",
	getAccessToken: () => localStorage.getItem('token'),
	toJSON: (res) => res.json()
}

const colorRandomizer = () => {
	return Math.floor(Math.random()*16777215).toString(16)
};

export { colorRandomizer };
